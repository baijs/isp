package com.xzsoft.isp.sms.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.xzsoft.isp.sms.entity.MessageTemplate;
import com.xzsoft.isp.sms.entity.MessageType;


public class MessageTemplateController {

	List<MessageTemplate> lm = new ArrayList<MessageTemplate>();
	MessageTemplate mt = new MessageTemplate();
	/**
	 * 
	 * @param templatename
	 * @return 根据名称查找模板信息
	 */
	@GetMapping("messageTemplateSimple/{templatename}")
	public List<MessageTemplate> findByName(@PathVariable String templatename){

		return lm;

	}
	/**
	 * 
	 * @return 是否添加成功
	 */
	@GetMapping("messageTemplateAdd/{messagetypename}_{templatecode}_{templatename}_{templateContent}")
	public String add(@PathVariable String messagetypename,
			@PathVariable String templatecode,@PathVariable String templatename,@PathVariable String templatecontent){
		return "添加成功";
	}
	/**
	 * 
	 * @param 修改后的消息类型名称
	 * @return 是否修改成功
	 */
	@GetMapping("templateUpdate/{messagetypename}_{templatename}_{templatecontent}")
	public String update(@PathVariable String messagetypename,@PathVariable String templatename,
			@PathVariable String templatecontent){

		return "修改成功";


	}
	/**
	 * 
	 * @param 要修改的消息类型的id
	 * @return 修改前消息类的信息
	 */
	@GetMapping("templateChange/{id}")
	public MessageType change(@PathVariable Long id){
		MessageType m=new MessageType();
		return m;

	}
	/**
	 * 
	 * @param 消息类型id
	 * @return 状态是否修改成功
	 */
	@GetMapping("templateChangeState/{id}")
	public String changeState(@PathVariable Long id){

		return "操作成功";


	}
	/**
	 * 
	 * @return 所有消息类型信息
	 */
	@GetMapping("templateAllSearch")
	public List<MessageTemplate> messageTypeAllSearch(){
		return lm;

	}

}
