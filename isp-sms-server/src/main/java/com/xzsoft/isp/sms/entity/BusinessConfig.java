package com.xzsoft.isp.sms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * 
 * @author 业务模板配置表
 *
 */
@Entity
public class BusinessConfig {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;//自增id
	
	@Column
	private String businessname;//业务名称
	
	@Column
	private String templatename;//消息模板名称
	
	@Column
	private String state;//状态
	
	

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBusinessname() {
		return businessname;
	}

	public void setBusinessname(String businessname) {
		this.businessname = businessname;
	}


	public String getTemplatename() {
		return templatename;
	}

	public void setTemplatename(String templatename) {
		this.templatename = templatename;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}


}
