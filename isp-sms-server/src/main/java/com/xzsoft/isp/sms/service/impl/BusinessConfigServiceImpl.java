package com.xzsoft.isp.sms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xzsoft.isp.sms.entity.BusinessConfig;
import com.xzsoft.isp.sms.repository.BusinessConfigRepository;
import com.xzsoft.isp.sms.service.BusinessConfigService;

@Service
public class BusinessConfigServiceImpl implements BusinessConfigService{
	@Autowired
	private BusinessConfigRepository businessConfigRepository;
	
	
	public List<BusinessConfig> findByBusinessName(String businessname){
		return businessConfigRepository.findByBusinessname(businessname);	
	}
	@Override
	public void add() {
		BusinessConfig bc = new BusinessConfig();
		bc.setTemplatename("ee");
		bc.setBusinessname("eee");
		bc.setState("未使用");
		businessConfigRepository.save(bc);
		
	}
	@Override
	public void update(Long id) {
		BusinessConfig bc = businessConfigRepository.findOne(id);
		bc.setTemplatename("555");
		bc.setBusinessname("eee");
		businessConfigRepository.save(bc);

	}
	
	@Override
	public String changeState(Long id) {
		BusinessConfig bc = businessConfigRepository.findOne(id);
		String state = bc.getState();
		if (state.equals("未使用")) {
			businessConfigRepository.delete(id);
			return "删除成功";
		} else if(state.equals("已禁用")){
			bc.setState("已启用");
			businessConfigRepository.save(bc);
			return "启用成功";
		}else if(state.equals("已启用")){
			bc.setState("已禁用");
			businessConfigRepository.save(bc);
			return "禁用成功";
		}
		return "状态不正确";
		
		
	}
	
	@Override
	public BusinessConfig findByBusinessConfigId(Long id) {
		
		return businessConfigRepository.findOne(id);
	}
	@Override
	public List<BusinessConfig> findByTemplateName(String templatename) {
		
		return businessConfigRepository.findByTemplatename(templatename);
	}
	@Override
	public List<BusinessConfig> findAll() {
		
		return businessConfigRepository.findAll();
	}
	

}
