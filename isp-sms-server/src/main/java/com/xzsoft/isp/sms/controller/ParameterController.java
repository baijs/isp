package com.xzsoft.isp.sms.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.xzsoft.isp.sms.entity.Parameter;
/**
 * 
 * @author 参数管理操作
 *
 */
@RestController
public class ParameterController {
	List<Parameter> lp = new ArrayList<Parameter>();
	Parameter p = new Parameter();
	/**
	 * 
	 * @param parametername
	 * @return 根据名称查找参数
	 */
	@GetMapping("parameterSimple/{parametername}")
	public List<Parameter> findByName(@PathVariable String parametername){
		
		return lp;

	}
	/**
	 * 
	 * @return 是否添加成功
	 */
	@GetMapping("parameterAdd/{parametername}_{parametercode}_{messagetypename}")
	public String add(){
		return "添加成功";
	}
	/**
	 * 
	 * @param1 修改后的参数名称
	 * @param2修改后的消息类型名称
	 * @return 是否修改成功
	 */
	@GetMapping("parameterUpdate/{parametername}_{messagetypename}")
	public String update(@PathVariable String parametername,@PathVariable String messagetypename){
		
			return "修改成功";
		

	}
	/**
	 * 
	 * @param 要修改的参数id
	 * @return 修改前参数的信息
	 */
	@GetMapping("parameterChange/{id}")
	public Parameter change(@PathVariable Long id){
			return p;

	}
	/**
	 * 
	 * @param 参数id
	 * @return 状态是否修改成功
	 */
	@GetMapping("parameterChangeState/{id}")
	public String changeState(@PathVariable Long id){
		
			return "操作成功";
		

	}
	/**
	 * 
	 * @return 所有消息类型信息
	 */
	@GetMapping("parameterAllSearch")
	public List<Parameter> AllSearch(){
		return lp;

	}

	

}
