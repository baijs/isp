package com.xzsoft.isp.sms.entity;

public class MessageTemplate {
	private Long id;
	private String templatename;
	private String templatecode;
	private String messagetypename;
	private String templateContent;
	private String state;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTemplatename() {
		return templatename;
	}
	public void setTemplatename(String templatename) {
		this.templatename = templatename;
	}
	public String getTemplatecode() {
		return templatecode;
	}
	public void setTemplatecode(String templatecode) {
		this.templatecode = templatecode;
	}
	public String getMessagetypename() {
		return messagetypename;
	}
	public void setMessagetypename(String messagetypename) {
		this.messagetypename = messagetypename;
	}
	public String getTemplateContent() {
		return templateContent;
	}
	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	

}
