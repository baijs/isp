package com.xzsoft.isp.sms.service;

import java.util.List;

import com.xzsoft.isp.sms.entity.Business;

public interface BusinessService {
	public List<Business> findByName(String businessname);
	
	public Business findById(Long id);
	
	public void add();
	
	public void update(Long id);
	
	public String changeState(Long id);
	
	public String allChangeState(Long[] a);
	
	public int countEnable();
	
	public List<Business> findAll();
	

}
