package com.xzsoft.isp.sms.service;

import java.util.List;

import com.xzsoft.isp.sms.entity.BusinessConfig;

public interface BusinessConfigService {
	public List<BusinessConfig> findByBusinessName(String businessname);
	
	public List<BusinessConfig> findByTemplateName(String templatename);
	
	public BusinessConfig findByBusinessConfigId(Long id);
	
	public void add();
	
	public void update(Long id);
	
	public String changeState(Long id);
	
	public List<BusinessConfig> findAll();
	 

}
