package com.xzsoft.isp.sms.entity;
/**
 * 
 * @author 参数表
 *
 */
public class Parameter {
	private Long id;//自增id
	
	private String parametername;//参数名称
	
	private String parametercode;//参数编码
	
	private String messagetypename;//消息类型名称
	
	private String state;//状态

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getParametername() {
		return parametername;
	}

	public void setParametername(String parametername) {
		this.parametername = parametername;
	}

	public String getParametercode() {
		return parametercode;
	}

	public void setParametercode(String parametercode) {
		this.parametercode = parametercode;
	}

	public String getMessagetypename() {
		return messagetypename;
	}

	public void setMessagetypename(String messagetypename) {
		this.messagetypename = messagetypename;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	
	
	

}
