package com.xzsoft.isp.sms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xzsoft.isp.sms.entity.Business;
import com.xzsoft.isp.sms.repository.BusinessRepository;
import com.xzsoft.isp.sms.service.BusinessService;

@Service
public class BusinessServiceImpl implements BusinessService{
	@Autowired
	private BusinessRepository businessRepository;
	
	
	public List<Business> findByName(String businessname){
		return businessRepository.findByBusinessname(businessname);	
	}
	@Override
	public void add() {
		Business bn = new Business();
		bn.setBusinesscode("444");
		bn.setBusinessname("ddd");
		bn.setState("未使用");
		businessRepository.save(bn);
		
	}
	@Override
	public void update(Long id) {
		Business bn = businessRepository.findOne(id);
		bn.setBusinesscode("555");
		bn.setBusinessname("eee");
		businessRepository.save(bn);

	}
	@Override
	public int countEnable() {
		
		
		return 0;
		

	}
	@Override
	public String changeState(Long id) {
		Business bn = businessRepository.findOne(id);
		String state = bn.getState();
		if (state.equals("未使用")) {
			businessRepository.delete(id);
			return "删除成功";
		} else if(state.equals("已禁用")){
			bn.setState("已启用");
			businessRepository.save(bn);
			return "启用成功";
		}else if(state.equals("已启用")){
			bn.setState("已禁用");
			businessRepository.save(bn);
			return "禁用成功";
		}
		return "状态不正确";
		
		
	}
	@Override
	public String allChangeState(Long[] a) {
		try {
			for (int i = 0; i < a.length;i++) {
				Business bn = businessRepository.findOne(a[i]);
				String state = bn.getState();
				if (state.equals("未使用")) {
					businessRepository.delete(a[i]);
				} else if(state.equals("已禁用")){
					bn.setState("已启用");
					businessRepository.save(bn);	
				}else if(state.equals("已启用")){
					bn.setState("已禁用");
					businessRepository.save(bn);
				}
			}
			return "操作成功";
		} catch (Exception e) {
			e.printStackTrace();
			return "操作失败";
		}
		
	}
	@Override
	public Business findById(Long id) {
		
		return businessRepository.findOne(id);
	}
	
	@Override
	public List<Business> findAll() {
		
		return businessRepository.findAll();
	}
	

}
