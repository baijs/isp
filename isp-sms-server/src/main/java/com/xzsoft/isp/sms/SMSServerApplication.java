package com.xzsoft.isp.sms;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;



/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-05-25 12:44
 */
@EnableDiscoveryClient  //激活eureka中的DiscoveryClient实现
@EnableEurekaClient
//@EnableHystrix
@SpringBootApplication
//@ServletComponentScan("com.github.wxiaoqi.security.admin.config.druid")
//@EnableRedisHttpSession
public class SMSServerApplication {
//	
	
	
	
	public static void main(String[] args) {
		SpringApplication.run(SMSServerApplication.class, args);
	}
}