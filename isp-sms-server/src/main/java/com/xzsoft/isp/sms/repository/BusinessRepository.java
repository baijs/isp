package com.xzsoft.isp.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xzsoft.isp.sms.entity.Business;
@Repository
public interface BusinessRepository extends JpaRepository<Business, Long>{
	public List<Business> findByBusinessname(String businessname);
	
	

}
