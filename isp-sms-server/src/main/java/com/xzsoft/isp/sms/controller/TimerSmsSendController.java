package com.xzsoft.isp.sms.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.xzsoft.isp.sms.entity.MessageType;
import com.xzsoft.isp.sms.entity.TimerSmsSend;

public class TimerSmsSendController {
	List<TimerSmsSend> lt = new ArrayList<TimerSmsSend>();
	TimerSmsSend t = new TimerSmsSend();
	/**
	 * 
	 * @param timersmssendname
	 * @return 根据名称查找
	 */
	@GetMapping("timerSmsSendSimple/{timersmssendname}")
	public List<TimerSmsSend> findByName(@PathVariable String timersmssendname){		
		return lt;
	}
	/**
	 * @param timersmssendname 定时任务名称
	 * @param sendtime 发送时间
	 * @param createtime 创建时间
	 * @param businessid 业务id
	 * @param sendtype 发送类型
	 * @param orgname 单位名称
	 * @param name 职工姓名
	 * @param mobile 职工电话
	 * @return 是否添加成功
	 */
	@GetMapping("timerSmsSendAdd/{timersmssendname}_{sendtime}_{createtime}_{businessid}_{sendtype}_{orgname}_{name}_{mobile}")
	public String add(@PathVariable String timersmssendname,@PathVariable String sendtime,@PathVariable String createtime,
			@PathVariable String businessid,@PathVariable String sendtype,@PathVariable String orgname,@PathVariable String name,
			@PathVariable String mobile){
		return "添加成功";
	}
	/**
	 * 
	 * @param timersmssendname 定时任务名称
	 * @param sendtime 发送时间
	 * @param createtime 创建时间
	 * @param businessid 业务id
	 * @param sendtype 发送类型
	 * @param orgname 单位名称
	 * @param name 职工姓名
	 * @param mobile 职工电话
	 * @return 是否修改成功
	 */
	@GetMapping("timerSmsSendUpdate/{timersmssendname}_{sendtime}_{createtime}_{businessid}_{sendtype}_{orgname}_{name}_{mobile}")
	public String update(@PathVariable String timersmssendname,@PathVariable String sendtime,@PathVariable String createtime,
			@PathVariable String businessid,@PathVariable String sendtype,@PathVariable String orgname,@PathVariable String name,
			@PathVariable String mobile){
		
			return "修改成功";
		

	}
	/**
	 * 
	 * @param 要修改的消息类型的id
	 * @return 修改前消息类的信息
	 */
	@GetMapping("messageTypeChange/{id}")
	public TimerSmsSend change(@PathVariable Long id){
			return t;

	}
	/**
	 * 
	 * @param 消息类型id
	 * @return 状态是否修改成功
	 */
	@GetMapping("messageTypeChangeState/{id}")
	public String changeState(@PathVariable Long id){
		
			return "操作成功";
		

	}
	/**
	 * 
	 * @return 所有消息类型信息
	 */
	@GetMapping("messageTypeAllSearch")
	public List<TimerSmsSend> AllSearch(){
		return lt;

	}


}
