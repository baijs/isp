package com.xzsoft.isp.sms.entity;

public class TimerSmsSend {
	private Long id;
	private String timersmssendname;
	private String message;
	private String messagetemplatename;
	private String businessname;
	private String state;
	private String sendtime;
	private String createtime;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTimersmssendname() {
		return timersmssendname;
	}
	public void setTimersmssendname(String timersmssendname) {
		this.timersmssendname = timersmssendname;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessagetemplatename() {
		return messagetemplatename;
	}
	public void setMessagetemplatename(String messagetemplatename) {
		this.messagetemplatename = messagetemplatename;
	}
	public String getBusinessname() {
		return businessname;
	}
	public void setBusinessname(String businessname) {
		this.businessname = businessname;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getSendtime() {
		return sendtime;
	}
	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	
	
	

}
