package com.xzsoft.isp.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xzsoft.isp.sms.entity.BusinessConfig;
@Repository
public interface BusinessConfigRepository extends JpaRepository<BusinessConfig, Long>{
	public List<BusinessConfig> findByBusinessname(String businessname);
	public List<BusinessConfig> findByTemplatename(String templatename);
	
	

}
