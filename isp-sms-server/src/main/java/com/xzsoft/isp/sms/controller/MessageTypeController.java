package com.xzsoft.isp.sms.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.xzsoft.isp.sms.entity.MessageType;
/**
 * 
 * @author 消息类型管理操作
 *
 */
@RestController
public class MessageTypeController {
	List<MessageType> lm = new ArrayList<MessageType>();
	MessageType m = new MessageType();
	/**
	 * 
	 * @param messagetypename
	 * @return 根据名称查找到了消息类型
	 */
	@GetMapping("messageTypeSimple/{messagetypename}")
	public List<MessageType> findByName(@PathVariable String messagetypename){
		
		return lm;

	}
	/**
	 * 
	 * @return 是否添加成功
	 */
	@GetMapping("messageTypeAdd/{messagetypename}_{messagetypecode}")
	public String add(@PathVariable String messagetypename,@PathVariable String messagetypecode){
		return "添加成功";
	}
	/**
	 * 
	 * @param 修改后的消息类型名称
	 * @return 是否修改成功
	 */
	@GetMapping("messageTypeUpdate/{messagetypename}")
	public String update(@PathVariable String messagetypename){
		
			return "修改成功";
		

	}
	/**
	 * 
	 * @param 要修改的消息类型的id
	 * @return 修改前消息类的信息
	 */
	@GetMapping("messageTypeChange/{id}")
	public MessageType change(@PathVariable Long id){
			return m;

	}
	/**
	 * 
	 * @param 消息类型id
	 * @return 状态是否修改成功
	 */
	@GetMapping("messageTypeChangeState/{id}")
	public String changeState(@PathVariable Long id){
		
			return "操作成功";
		

	}
	/**
	 * 
	 * @return 所有消息类型信息
	 */
	@GetMapping("messageTypeAllSearch")
	public List<MessageType> messageTypeAllSearch(){
		return lm;

	}

}
