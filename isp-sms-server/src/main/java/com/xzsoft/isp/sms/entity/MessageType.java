package com.xzsoft.isp.sms.entity;
/**
 * 
 * @author 消息类型管理
 *
 */
public class MessageType {
	private Long id;//自增id
	private String messagetypename;//消息类型名称
	private String messagetypecode;//消息类型编码
	private String state;//消息类型状态
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMessagetypename() {
		return messagetypename;
	}
	public void setMessagetypename(String messagetypename) {
		this.messagetypename = messagetypename;
	}
	public String getMessagetypecode() {
		return messagetypecode;
	}
	public void setMessagetypecode(String messagetypecode) {
		this.messagetypecode = messagetypecode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	

}
