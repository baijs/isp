package com.xzsoft.isp.sms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.xzsoft.isp.sms.entity.BusinessConfig;
import com.xzsoft.isp.sms.service.BusinessConfigService;
/**
 * 
 * @author 业务模板配置
 *
 */
@RestController
public class BusinessConfigController {
	@Autowired
	private BusinessConfigService businessConfigService;
	/**
	 * 
	 * @param 业务名称
	 * @return 根据业务名称查询业务模板信息
	 */
	@GetMapping("businessConfigSimpleBusinessName/{businessname}")
	public List<BusinessConfig> findByBusinessName(@PathVariable String businessname){
		return businessConfigService.findByBusinessName(businessname);

	}
	/**
	 * 
	 * @param 消息名称
	 * @return 根据消息模板名称查询业务模板信息
	 */
	@GetMapping("businessConfigSimpleTemplateName/{templatename}")
	public List<BusinessConfig> findByTemplateName(@PathVariable String templatename){
		return businessConfigService.findByTemplateName(templatename);

	}
    /**
     * @param1 业务名称
     * @param2 消息模板名称
     * @return 业务模板是否添加成功
     */
	@GetMapping("businessConfigAdd/{businessname}_{templatename}")
	public String add(){
		try {
			businessConfigService.add();
			return "添加成功";
		} catch (Exception e) {
			e.printStackTrace();
			return "添加失败";
		}
	}
	/**
	 * 
	 * @param1 修改后的业务模板名称
	 * @param2 修改后的消息模板名称 
	 * @return 是否修改成功
	 */
	@GetMapping("businessConfigUpdate/{businessname}_{templatename}")
	public String update(@PathVariable String businessname,@PathVariable String templatename){
		
			return "修改成功";
	}
	/**
	 * 
	 * @param 要修改的业务模板id
	 * @return 修改前的业务模板信息
	 */
	@GetMapping("businessConfigChange/{id}")
	public BusinessConfig change(@PathVariable Long id){

		return businessConfigService.findByBusinessConfigId(id);
	
		}
	/**
	 * 
	 * @param 消息模板id
	 * @return 状态是否改变成功
	 */
	@GetMapping("businessConfigChangeState/{id}")
	public String changeState(@PathVariable Long id){
		try {
			return businessConfigService.changeState(id);
			
		} catch (Exception e) {
			e.printStackTrace();
			return "操作失败";
		}

	}
	/**
	 * 
	 * @return 查询所有业务模板信息
	 */
	@GetMapping("businessConfigAllSearch")
	public List<BusinessConfig> businessConfigAllSearch(){
		return businessConfigService.findAll();

	}
	
}