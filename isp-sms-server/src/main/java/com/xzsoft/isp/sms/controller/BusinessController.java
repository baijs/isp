package com.xzsoft.isp.sms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xzsoft.isp.sms.entity.Business;
import com.xzsoft.isp.sms.service.BusinessService;
/**
 * 
 * @author 业务管理操作
 *
 */
@RestController
public class BusinessController {
	@Autowired
	private BusinessService businessService;
	/**
	 * 
	 * @param 业务名称
	 * @return 根据业务名称查询业务信息
	 */
	@GetMapping("businessSimple/{businessname}")
	public List<Business> findByName(@PathVariable String businessname){
		return businessService.findByName(businessname);

	}
	/**
	 * 
	 * @业务是否添加成功
	 */
	@GetMapping("businessAdd/{Business}")
	public String add(@PathVariable Business business){
		try {
			
			businessService.add();
			return "添加成功";
		} catch (Exception e) {
			e.printStackTrace();
			return "添加失败";
		}
	}
	/**
	 * 
	 * @param 修改后的新业务名称
	 * @return 师傅修改成功
	 */

	@GetMapping("businessUpdate/{businessname}")
	public String update(@PathVariable String businessname){
		
			return "修改失败";

	}
	/**
	 * 
	 * @param 要修改的业务id
	 * @return 修改前业务的信息
	 */
	@GetMapping("businessChange/{id}")
	public Business change(@PathVariable Long id){
			return businessService.findById(id);

	}
	/**
	 * 
	 * @param 要更改状态的业务id
	 * @return 是否操作成功
	 */
	@GetMapping("businessChangeState/{id}")
	public String changeState(@PathVariable Long id){
		try {
			return businessService.changeState(id);

		} catch (Exception e) {
			e.printStackTrace();
			return "操作失败";
		}

	}
	/**
	 * 
	 * @return 所有业务的信息
	 */
	 @GetMapping("businessAllSearch")  
	public List<Business> AllSearch(){
		return businessService.findAll();

	}
	 
}