package com.xzsoft.isp.sms.entity;
/**
 * 
 * @author 网关配置
 *
 */
public class SmsGate {
	
	private Long id;//自增id
	private String url;//地址
	private String orgaccount;//企业账户
	private String password;//密码
	private String posttype;//传输方式
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getOrgaccount() {
		return orgaccount;
	}
	public void setOrgaccount(String orgaccount) {
		this.orgaccount = orgaccount;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPosttype() {
		return posttype;
	}
	public void setPosttype(String posttype) {
		this.posttype = posttype;
	}
	
	
	

}
